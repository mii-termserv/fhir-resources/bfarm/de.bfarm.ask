Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: bfarm-sutermserv-tags
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = #bfarm
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = #bfarm
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_license"
* ^meta.tag[=].code = #https://mii-termserv.de/licenses#bfarm
* ^meta.tag[=].display = "BfArM license terms"

CodeSystem: ASK
Title: "Arzneistoffkatalog"
Id: ask
Description: "Die ASK-Nummer wird zur Identifikation von Arzneistoffen verwendet. Der Katalog deckt alle Stoffe von Arzneimitteln ab, die in Deutschland zugelassen sind oder waren, und ermöglicht somit eine eindeutige Zuordnung aller Wirkstoffe"
* ^url = "http://fhir.de/CodeSystem/ask"
* ^version = "20240723"
* ^status = #active
* ^experimental = false
* ^content = #complete
* ^caseSensitive = false
* insert bfarm-sutermserv-tags
* ^property[+].code = #formelstamm
* ^property[=].description = "Formelstamm"
* ^property[=].type = #string
* ^property[+].code = #bruttoformel
* ^property[=].description = "Bruttoformel"
* ^property[=].type = #string
* ^copyright = "Bundesinstitut für Arzneimittel und Medizinprodukte (BfArM)"
* ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/cqf-scope"
* ^extension[=].valueString = "@mii-termserv/hl7.uv.genomics-reporting.terminology"
/*^babelfsh
xslt 
--xslt=./ask.xslt
--xml=./input-files/bezvo.xml
--filter-concepts-without-display-and-designations
--filter-duplicate-codes
^babelfsh*/
