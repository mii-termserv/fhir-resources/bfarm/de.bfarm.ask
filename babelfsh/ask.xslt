<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:array="http://www.w3.org/2005/xpath-functions/array"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                xmlns:math="http://www.w3.org/2005/xpath-functions/math"
                xmlns="urn:mii-termserv:xmlns:babelfsh:codesystem"
                expand-text="yes"
                version="3.0">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:template match="/" mode="#default">
        <CodeSystem name="ASK">
            <xsl:apply-templates select="/SUBSTANCES"/>
        </CodeSystem>
    </xsl:template>


    <xsl:template match="SUBSTANCES" mode="#all">
        <concepts>
            <xsl:apply-templates select="SUBSTANCE"/>
        </concepts>
    </xsl:template>
    
    <xsl:template match="SUBSTANCE" mode="#all">
        <xsl:if test="./CAS">
            <concept>
                <xsl:apply-templates select="ASK"/>
                <xsl:apply-templates select="HBEZ1"/>
                <xsl:apply-templates select="HBEZ2|HBEZ3"/>
                <xsl:apply-templates select="FOST"/>
                <xsl:apply-templates select="HBRUFO"/>
            </concept>
        </xsl:if>
    </xsl:template>

    <xsl:template match="ASK" mode="#default">
        <code><xsl:value-of select="."/></code>
    </xsl:template>
    
    <xsl:template match="HBEZ1" mode="#default">
        <xsl:if test=". != '-'">
            <display><xsl:value-of select="."/></display>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="HBEZ2|HBEZ3" mode="#default">
        <xsl:if test=". != '-'">
            <designation>
                <language>de</language>
                <use>
                    <system>http://snomed.info/sct</system>
                    <code>900000000000013009</code>
                    <display>Synonym</display>
                </use>
                <value><xsl:value-of select="."/></value>
            </designation>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="FOST" mode="#default">
        <property code="formelstamm">
            <valueString><xsl:value-of select="."/></valueString>
        </property>
    </xsl:template>

    <xsl:template match="HBRUFO" mode="#default">
        <property code="bruttoformel">
            <valueString><xsl:value-of select="."/></valueString>
        </property>
    </xsl:template>

</xsl:stylesheet>