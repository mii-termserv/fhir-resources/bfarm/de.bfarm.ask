# ASK from BfArM

This package distributes the ASK list from https://www.bfarm.de/DE/Arzneimittel/Arzneimittelinformationen/Arzneimittel-recherchieren/Stoffbezeichnungen/_node.html, generated from the `bezvo.xml` file using [BabelFSH](https://gitlab.com/mii-termserv/babelfsh) with an XML Stylesheet Transformation (XSLT).
